<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php storefront_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/custom.css" />

	<script src="//use.typekit.net/hgn5cnl.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php
	do_action( 'storefront_before_header' ); ?>

	<header id="masthead" class="site-header" role="banner" <?php if ( get_header_image() != '' ) { echo 'style="background-image: url(' . esc_url( get_header_image() ) . ');"'; } ?>>
		<div class="col-full">
			
			<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Navigation">
				<button class="menu-toggle">Navigation</button>
				<div class="menu">
					<ul aria-expanded="false" class=" nav-menu">
						<li class="current_page_item"><a href="<?php echo home_url(); ?>">Shop</a></li>
						<!--<li class="page_item page-item-2"><a href="http://localhost:8888/yes-please/sample-page/">Clothes</a></li>
						<li class="page_item page-item-2"><a href="http://localhost:8888/yes-please/sample-page/">Rings</a></li>
						<li class="page_item page-item-2"><a href="http://localhost:8888/yes-please/sample-page/">Tatts</a></li>-->
						<li class="page_item page-item-2"><a href="<?php echo home_url(); ?>/about/">About</a></li>
						<li class="page_item page-item-2"><a href="<?php echo home_url(); ?>/contact/">Contact</a></li>
						
					</ul>
				</div>
				<div class="menu">
					<ul>
						<li class="current_page_item"><a href="<?php echo home_url(); ?>">Home</a></li>
						<li class="page_item page-item-5"><a href="<?php echo home_url(); ?>/cart/">Cart</a></li>
						<li class="page_item page-item-6"><a href="<?php echo home_url(); ?>/checkout/">Checkout</a></li>
						<li class="page_item page-item-7"><a href="<?php echo home_url(); ?>/my-account/">My Account</a></li>
						<li class="page_item page-item-2"><a href="<?php echo home_url(); ?>/about/">About</a></li>
						<li class="page_item page-item-2"><a href="<?php echo home_url(); ?>/contact/">Contact</a></li>

					</ul>
					
				</div>
			
			</nav>
				<?php
			/**
			 * @hooked storefront_skip_links - 0
			 * @hooked storefront_social_icons - 10
			 * @hooked storefront_site_branding - 20
			 * @hooked storefront_secondary_navigation - 30
			 * @hooked storefront_product_search - 40
			 * @hooked storefront_primary_navigation - 50
			 * @hooked storefront_header_cart - 60
			 */
			do_action( 'storefront_header' ); ?>
			
			<a href="<?php echo home_url(); ?>"><h1 class="logo">Yes Please</h1></a>
			
			<!--<nav class="secondary-navigation" role="navigation" aria-label="Secondary Navigation">
				<ul>
					<li class="page_item page-item-5"><a href="http://localhost:8888/yes-please/cart/">Cart</a></li>
					<li class="page_item page-item-6"><a href="http://localhost:8888/yes-please/checkout/">Checkout</a></li>
					<li class="page_item page-item-7"><a href="http://localhost:8888/yes-please/my-account/">My Account</a></li>
				</ul>
			</nav>-->

			

		</div>
	</header><!-- #masthead -->

	<?php
	/**
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

	<div id="content" class="site-content" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' ); ?>
